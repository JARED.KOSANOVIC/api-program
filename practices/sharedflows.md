# Shared Flows

Key features of Sharedflows:
- “Reusability and Consistency”
- Straight line flow
- Injection not callout
- Not opaque: Shared Flow can set/get all flow variables in the proxy they are injected into.



Different Sharedflows have different responsibilities.

Key responsibilities of Shared flows:
- Key Value Map interaction
- Security (Apikey and Oauth)
- Spike Arrest
- Quota
- Threat Detection (Message Size)
- Redaction (Remove inbound security)
- Dynamic Routing (Rewrite Target)



1. KVMs drive the runtime behaviour of the framework

    Example: AP1_V1 - 
    ```
    {
        "Base":"http://mocktarget.apigee.net",
        "MaximumMessageSize":"1000",
        "Security":"oauth2",
        "Spike":"1000pm",
        "SpikeEnabled":"false",
        "QuotaEnabled":"false",
        "ScrubCreds":"true"
    }
    ```
2. Security Shared Flow uses conditions to execute only option chosen via KVM.

     Example: API_V1 - "Security":"oauthcc"
    
     Security value "apikey" checks the parsed username while security value "bypass" skips the security check

3. Spike arrests protect the backend API

    Example: API_V1 - "Spike":"1000pm","SpikeEnabled":"false"

4. Quotas limit access to a API for a given consumer of a Product

    Example: API_V1 - "QuotaEnabled":"false"

5. Threat Detection comes in many forms Message Size, Composition, Depth

    Example: API_V1: "MaximumMessageSize":"1000"

6. Redaction is a form of transformation including add, remove, modify of Request and/or Response

    Example: API_V1 : "ScrubCreds":"true"

7. Framework makes properties available to proxy but does not execute. Execution is the responsibility of the proxy pipeline in Target PreFlow as a Shared Flow.

    Example: API_V1 - "Base":"http://mocktarget.apigee.net"

 
