# UW API Program Governance Interim Team

## Background and Business Need

UW-Madison is establishing an API Program, defined as an ecosystem and culture of API-based integration across our institution.  The API Program will encompass the collection of APIs that expose institutional data domains, and APIs that represent myriad distributed business functions, all served by a centrally-offered platform.  The Program will define policies, practices, and patterns that govern publishing and consumption of APIs and that will guide the community as we establish and curate the UW’s API ecosystem.

Because of the breadth of the Program, governance is needed to define and ensure adherence to consistent policies, practices, patterns, security, developer and user experiences, and implementation and curation of corresponding technical controls within the API management platform (Apigee).  The API Program Governance Team should consist of a cross-functional group of leadership and subject-matter experts with a stake in the success and longevity of the API Program.  This cross-functional team should share a commitment to the mindset and value of API-based integration and have inputs to related institutional policies (e.g. security and data management), influence over budgets, organizational pull, and familiarity with UW-Madison’s unique institutional landscape and challenges.

## Interim Team

The API Program is intended to be broad, representative of participants and stakeholders across the UW and through each level of leadership.  Establishing a program of this scale, with appropriate buy-in, will be challenging.  This is particularly the case at UW-Madison in which a distributed or “federated” IT model is the norm, and no single executive can unilaterally push such a program to success.  

As such, we will use an incremental approach to establishing the API Program.  This will start with an Interim Governance Team that will convene throughout and likely beyond Apigee implementation (~July to September 2021).  This team will be made up of participants with a stake in the Program’s success, subject-matter experts in API design and implementation, cybersecurity, data management, and representatives of the DoIT team that will operate Apigee.

The interim governance team will make operational decisions that are necessary to successfully implement the nascent API Program and Apigee, and its decisions shall be considered provisional until a formal governance team is created.  The charter for the formal team will be created and recommended by the interim governing team.  A formal team that operates with executive sponsorship will ultimately be needed for the broad and long-term success of the API Program at UW-Madison.

## Objectives

Support the API Program by defining, implementing, enforcing, curating, and communicating policies, practices, and patterns.
Policies
Policies are rules that are agreed upon by API Program governance and implemented in the API Gateway (Apigee) through technical controls.  They will be technically enforced in the API Gateway, and will be monitored and enforced.  Policies without a technical control will be relegated to practices or will be governed outside of the API Program (e.g. UW-Madison Policy Library).
Practices
Practices are opinionated ways of doing things to help API publishers and consumers go about the business of creating, publishing, managing, and consuming APIs.  These practices don't rise to the level of policies (things that are agreed upon by governance, monitored, and enforced), but are important for publishers to understand and implement.

Consistent practices help to improve the overall user and developer experiences as the ecosystem of APIs grows over time.  Publishing and consuming APIs is by nature a community activity, so it is important for stakeholders to adhere to and help update this documentation.
Patterns
Patterns are themes, trends, and collections of practices that are used broadly, usually well beyond the scope of UW-Madison.  Examples might include links and/or documentation about technology and architectural patterns, industry trends for APIs, and where they might have advantages or disadvantages.

## Interim Team Roles & Responsibilities
| Role | Responsibilities |
|------| ---------------- |
| Program Leadership | <ul><li>Organize the interim API Program Governance Team</li><li>Facilitate governance meetings</li><li>Own decision-making rules and practices for the team</li><li>Communicate decisions and changes to the API Program community</li><li>Manage the curation of API Program documentation</li><li>Guide the creation and recommendation of a charter for the formal API Program Governance Team</li></ul> |
| Team members | <ul><li>Provide input to decision-making rules and practices of the team</li><li>Advocate for the needs of the API Program community and its business objectives</li><li>Create, review, approve, and advocate for policies, practices, and patterns needed for API Program success</li><li>Approve onboarding and publishing of new APIs</li></ul> |

A detailed RACI chart for the proposed roles and responsibilities of the API Program is available [here](raci.md).

## Interim Team Logistics
Members of the API Program Governance Interim Team are asked for consistent, active participation in weekly meetings throughout the Apigee implementation (through September 2021).  The chairpeople will assemble topics and decisions to be made based on the needs of the implementation team and with input from interim team members and Interoperability sponsorship.

## Sample Governance Decisions To Be Made

After organizing and adopting operating procedures, the API Program Governance Interim Team will work through a backlog of Governance issues identified by the team and our partners to reach provisional decisions.  Additional decisions will be identified throughout implementation.  This team will also create and recommend a charter for the long-term, formal API Program Governance Team as a better understanding of needs emerges throughout implementation.
