# RACI matrix for API Program at UW-Madison

The [API Program RACI matrix](https://docs.google.com/spreadsheets/d/152o6fscO3N-0qxsifPhjkro5Fs8uTpEEW_qFCe_c2Yc/edit?usp=sharing) documents the deliverables or activities for each of the roles within the program. We use the following definitions:
- R=Responsible for doing the work
- A=Single person accountable for the work
- C=Consulted about the work
- I=Informed about the work

Further information on the role distinctions present in the RACI matrix can be found [here](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix#Role_distinction).



