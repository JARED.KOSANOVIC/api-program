# API Program Governance

This repository houses governance documentation for UW-Madison's API Program -- the policies, practices, and patterns relating to APIs expressed through the UW API gateway (Apigee).

## About 

The audience for this repository includes anyone who is impacted by the policies, practices, and patterns documented herein. Broadly speaking, that likely means anyone who is consuming and creating APIs, and stakeholders with an interest in API- and event-based integration (e.g. business units that integrate with enterprise data, security professionals, data custodians and policy-makers, etc.). This repository will be the source of truth and the historical record for decisions related to UW's API Program, defined as the emerging ecosystem and culture of API- and event-based integration on our campus. 

The API Program team is made up of people with a vested interest in the success of growing this ecosystem and culture, and who will guide policies that are implemented as technical controls in the API management platform. The API Program team will contribute, discuss, and formalize policies, practices, and patterns within this repository. The repository will be public to the extent possible to ensure transparency of the team's work, debate, and overall decision-making process.

More information about the API Program team can be found in our [charter](charter.md) and [roles and responsibilities matrix](raci.md).

## Adding to this Repository

Changes to content in this respository can be made by anyone on the API Program team. We will use the practices below when contributing, editing, discussing, and merging content. We will iterate on our processes and documentation, changing them as we learn more about what works well and what doesn't.

- When adding or editing any content, first create a new branch of of the `main` one. Use this to store changes, and commit to the repository often (this is like saving your work).
- When you are ready to integrate your content into the `main` branch, create a merge request. If you want others to review your work in progress, preface the merge request with `WIP:` so others know this is not finalized.
- Active discussion and edits from others are encouraged to enrich content with multiple perspectives. Comments may be made on the overview tab of a merge request, and also directly in the content (e.g. markdown documentation). Use each based on the specificity of your feedback.
- Commit messages should be as descriptive as possible so that people reviewing content know what was changed and can easily target feedback.
- Approval rules on merge requests should be commensurate with the impact of the change. Minor edits (e.g. fixing typos, clarifying content, etc.) may only need a couple approvers from the API Program team.
- Things like policies or major changes to patterns and practices should use approval rules that require a majority of the API Program team at minimum before the content can be merged.

Helpful documentation:

- [Logging into Gitlab](https://kb.wisc.edu/57963)
- [Creating a branch](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch)
- [Using the Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/)
- [Creating merge requests](https://docs.gitlab.com/12.10/ee/user/project/merge_requests/creating_merge_requests.html)
- [Markdown cheat sheat](https://www.markdownguide.org/cheat-sheet/)

## Repository Contents

- [Policies](policies/README.md)
- [Practices](practices/README.md)
- [Patterns](patterns/README.md)
- [API Program Team charter](charter.md)
- [API Program Team roles and responsibilities RACI](raci.md)
- [Glossary](glossary.md)

## Contact

To contact the API Program team, send an email to: [apiprogramteam@office365.wisc.edu](mailto:apiprogramteam@office365.wisc.edu)